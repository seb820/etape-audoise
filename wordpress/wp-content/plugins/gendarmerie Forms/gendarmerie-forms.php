<?php
/**
 * Plugin Name: Gendarmerie Forms Plugin
 * Text Domain: gendarmerie-form
 */

// Include the form builder class file
require_once(__DIR__ . '/includes/classBuilder.php');

// Check if the main plugin class already exists
if (!class_exists('GendarmerieForms')) {

    class GendarmerieForms
    {
        public function __construct()
        {
            // Register the shortcode and form handler
            add_shortcode('gendarmerieform', array($this, 'form'));
            add_action('admin_post_gendarmerie_contact_form', array($this, 'form_handler'));

            // Enqueue scripts and styles
            add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        }

        public function enqueue_scripts()
        {
            // Enqueue your CSS and JavaScript files here
            wp_enqueue_style('gendarmerie-forms-style', plugin_dir_url(__FILE__) . 'css/style.css');
            wp_enqueue_script('gendarmerie-forms-script', plugin_dir_url(__FILE__) . 'js/script.js', array('jquery'), '1.0', true);
        }

        public function form($atts)
        {
            // Extract shortcode attributes
            $atts = shortcode_atts(
                array(
                    'add_honeypot' => 'false',
                ),
                $atts,
                'gendarmerieForm'
            );

            // Instantiate the form builder class (replace FormBuilderClass with the actual class name)
            $form_builder = new PhpFormBuilder();

            // Set form options
            $form_builder->set_att('action', esc_url(admin_url('admin-post.php')));
            $form_builder->set_att('add_honeypot', $atts['add_honeypot']);

            // Add form inputs
            $form_builder->add_input('action', array(
                'type'  => 'hidden',
                'value' => 'gendarmerie_contact_form',
            ), 'action');

            $form_builder->add_input('wp_nonce', array(
                'type'  => 'hidden',
                'value' => wp_create_nonce('submit_contact_form'),
            ), 'wp_nonce');

            $form_builder->add_input('redirect_id', array(
                'type'  => 'hidden',
                'value' => get_the_ID(),
            ), 'redirect_id');

            $form_builder->add_input('Name', array(
                'type'        => 'text',
                'placeholder' => 'Enter your name',
                'required'    => true,
            ), 'name');

            $form_builder->add_input('Email', array(
                'type'        => 'email',
                'placeholder' => 'Enter your email',
                'required'    => true,
            ), 'email');

            $form_builder->add_input('Message', array(
                'type'        => 'textarea',
                'placeholder' => 'Enter your message',
                'required'    => true,
            ), 'message');

            // Start output buffering
            ob_start();

            // Display success message if present
            $status = filter_input(INPUT_GET, 'status', FILTER_VALIDATE_INT);
            if ($status == 1) {
                printf('<div class="success-message"><p>%s</p></div>', __('Message submitted successfully', 'gendarmerie-form'));
            }

            // Render the form
            $form_builder->build_form();

            // Get the buffered content
            $output = ob_get_clean();

            // Return the form output
            return $output;
        }
        public function form_handler()
        {
            if (isset($_POST['action']) && $_POST['action'] === 'gendarmerie_contact_form') {
                // Verify the form submission
                if (!isset($_POST['wp_nonce']) || !wp_verify_nonce($_POST['wp_nonce'], 'submit_contact_form')) {
                    // Invalid nonce, handle the error here
                    wp_die('Invalid form submission');
                }
        
                // Validate and sanitize the form inputs
                $name    = sanitize_text_field($_POST['name']);
                $email   = sanitize_email($_POST['email']);
                $message = sanitize_textarea_field($_POST['message']);
        
                // Create a new post with the form data
                $post_data = array(
                    'post_title'   => 'New messages',
                    'post_content' => "Name: $name\nEmail: $email\nMessage: $message",
                    'post_status'  => 'publish',
                    'post_type'    => 'post',
                );
                $post_id = wp_insert_post($post_data);
        
                if ($post_id) {
                    // Display the success message
                    $success_message = __('Message sent successfully!', 'gendarmerie-form');
                    $output = sprintf('<div class="success-message" style="background-color: green; padding: 10px; color: white;">%s</div>', $success_message);
        
                    // Get the contact form page URL dynamically
                    $contact_form_url = get_permalink(get_page_by_path('contact'));
        
                    // Add a "Back to Contact Form" button
                    $output .= sprintf('<a href="%s" class="back-to-form">%s</a>', esc_url($contact_form_url), __('Back to Contact Form', 'gendarmerie-form'));
        
                    echo $output;
                } else {
                    // Error occurred while creating the post, handle the error here
                    $error_message = __('An error occurred while submitting the form.', 'gendarmerie-form');
                    $output = sprintf('<div class="error-message" style="background-color: red; padding: 10px; color: white;">%s</div>', $error_message);
                    echo $output;
                }
        
                // Clear the form input values
                $_POST['name'] = '';
                $_POST['email'] = '';
                $_POST['message'] = '';
            }
        }
        
        
        
    }
}

// Instantiate the GendarmerieForms class
$gendarmerieForms = new GendarmerieForms();
