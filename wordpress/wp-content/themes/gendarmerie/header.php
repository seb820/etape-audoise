
<head>
    <!-- <meta charset="<?php bloginfo('charset'); ?>"> -->
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
        <div id="overlay">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'top_bar_menu',
                'container' => false,
                'items_wrap' => '%3$s',
            ));
            ?>
        </div>
        <div class="nav">
            <div class="logo">
                <?php
                if (function_exists('the_custom_logo')) {
                    $custom_logo_id = get_theme_mod('custom_logo');
                    $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    if (has_custom_logo()) {
                        echo '<img src="' . esc_url($logo[0]) . '" alt="Logo">';
                    } else {
                        echo 'LOGO';
                    }
                }
                ?>
            </div>
            <div id="hamburger">
                <div></div>
            </div>
        </div>
    </header>
<script>
    (() => {
    const hamburger = document.getElementById("hamburger");
    const menu = document.getElementById("overlay");
    let closed = true;
  
    const change = () => {
      if (closed) {
        hamburger.classList.add("open");
        menu.classList.add("menu");
        menu.style.transform = "translateX(0%)"; 
      } else {
        hamburger.classList.remove("open");
        menu.classList.remove("menu");
        menu.style.transform = "translateX(100%)"; 
      }
      closed = !closed;
    };
  
    hamburger.addEventListener("click", change);
  })();
  
</script>

   


