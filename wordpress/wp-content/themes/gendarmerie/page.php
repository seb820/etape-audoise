<?php get_header(); ?>

<div>
    <h1><?php the_title(); ?></h1>

    <div>
        <?php
        // Start the loop
        if (have_posts()) :
            while (have_posts()) :
                the_post();

                // Display the content of the page
                the_content();

            endwhile;
        endif;
        ?>
    </div>
</div>

<?php get_footer(); ?>
