<?php get_header(); ?>

<section class="page-wrap">
    <div class="container-single-chambre">
        <div class="chamber_container1">
            <div class="text_zone">
                <h2><?php the_title(); ?></h2>
                <p><?php the_field('chambre_description'); ?></p>
                <a href="https://bookingpremium.secureholiday.net/en/16920/search/product-view/124266" class="btn-resa">Réservez</a>
            </div>
            <div class="main_picture">
                <?php
                $chambre_image = get_field('chambre_image');
                if ($chambre_image) : ?>
                    <img src="<?php echo $chambre_image; ?>" alt="<?php the_title(); ?>">
                <?php endif; ?>
                <p> Lit 2 places</p>
            </div>
        </div>

        <div class="chamber_container2">
            <div class="petite-image">
                <img src="<?php echo get_template_directory_uri(); ?>/image/sdb2.JPG" class="img-petite" alt="dortoire 4 personnes">
                <p> Salle de bain</p>
            </div>
            <div class="image-moyenne">
                <img src="<?php echo get_template_directory_uri(); ?>/image/terassecheval.JPG" class="img-moyenne" alt="dortoire 4 personnes">
                <p> Vue de la terrasse</p>
            </div>
        </div>
    </div>

    <div class="comments-section">
        <?php
        // Get only the approved comments
        $args = array(
            'status' => 'approve',
            'post_id' => get_the_ID(),
        );

        // The comment Query
        $comments_query = new WP_Comment_Query;
        $comments = $comments_query->query($args);

        // Comment Loop
        if ($comments) {
            foreach ($comments as $comment) {
                echo '<p>' . $comment->comment_content . '</p>';
                echo '<h4>' . $comment->comment_author . '</h4>';
            }
        } else {
            echo 'No comments found.';
        }
        ?>

        <?php comments_template(); ?>
    </div>
</section>

<?php get_footer(); ?>
