<footer class="footer">
  <div class="container-footer">
    <h3>Contact</h3>
    <div class="row">
      <div class="col-md-4">
        <div class="footer-box">
          <h4 class="footer-header">Booking</h4>
          <ul class="list-unstyled">
            <li><a href="tel:000000000000"><i class="fas fa-phone-alt"></i> 0000000000000000</a></li>
            <li><a href="https://maps.google.com/?q=345 route de cuxac 11100 france"><i class="fas fa-map-marker-alt"></i> 345 route de cuxac</a></li>
            <li><a href="mailto:hello@reallygreatsite.com"><i class="fas fa-envelope"></i> hello@reallygreatsite.com</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <div class="footer-box">
          <h4 id="opening-hours" class="footer-header">Opening Hours</h4>
          <p>Monday to Friday: 9:00am - 6:00pm</p>
          <p>Saturday: 9:00am - 12:00pm</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="footer-box">
          <h4 class="footer-header">Connect with Us</h4>
          <ul class="list-inline">
            <li><a href="https://www.facebook.com/YOUR_FACEBOOK_PAGE"><i class="fab fa-facebook"></i></a>gendarmerie/facebook.com</li>
            <li><a href="https://www.twitter.com/YOUR_TWITTER_PROFILE"><i class="fab fa-twitter"></i></a>greatsite/twitter.com</li>
            <li><a href="https://www.instagram.com/YOUR_INSTAGRAM_PROFILE"><i class="fab fa-instagram"></i></a>gendarmerie_123/instagram.com</li>
          </ul>
          <p class="tag-us">Tag us in your photos !</p>
        </div>
      </div>
    </div>
  </div>
</footer>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">

<?php wp_footer(); ?>
</body>
</html>