<?php 
/* Template Name: about
*/
?>
<?php get_header();?>

<div>  
<!-- <h1> <?php the_title();?> </h1> -->

<div id = "about-usContainer">
<div class="container1" >
        <div class="zonetexte1">
            <H2> Notre Gîte</H2>
            <p> A seulement 20 minutes de Carcassonne, situé aux portes de la Montagne Noire, notre gîte d'étapes est parfaitement située pour découvrir notre belle Aude. 
                Comptant 5 chambres de 4 personnes, notre Gîte offre une capacité d'accueil totale de 20 personnes. 
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. A corrupti eos, cum accusamus earum fugiat dolore ea accusantium perferendis, corporis, voluptates enim. Doloribus quisquam ducimus repudiandae nulla blanditiis qui optio.</p>

        </div>
        <div class ='zoneimage1'>
            <img  class="zoneimage1-img" src = "http://localhost/wordpress/wp-content/uploads/2023/07/gite_gendarmeri_exterieur.png">

        </div>

    </div>

    <div class="container2" >
        <div class="zonetexte1">
          <h2> La cuisine commune</h2>
            <p> Nous vous proposons une cuisine spacieuse, et lumineuse. Equipée d'un frigo, de plaque, d'un four ainsi que caffetiére et bouilloire </p>

        </div>
        <div class ='zoneimage1'>
            <img class="zoneimage1-img" src="http://localhost/wordpress/wp-content/uploads/2023/07/cuisine1.jpg">

        </div>

    </div>

    <div class="container1" >
        <div class="zonetexte1">
            <H2> Le salon</H2>
            <p> Ouvert sur l'espace exterieur, c'est l'endroit idéal pour se reposer et chiller aprés vos balades et activités de la journée
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. A corrupti eos, cum accusamus earum fugiat dolore ea accusantium perferendis, corporis, voluptates enim. Doloribus quisquam ducimus repudiandae nulla blanditiis qui optio.</p>

        </div>
        <div class ='zoneimage1'>
            <img class="zoneimage1-img" src = "http://localhost/wordpress/wp-content/uploads/2023/07/salon2.jpg">

        </div>

    </div>

    <div class="container2" >
        <div class="zonetexte1">
          <h2> L'extérieur</h2>
            <p> Pour les belles soirées d'étè, vous disposerez également d'une cours aménagé avec un barbecue et de quoi se détendre. Vous pourrez également profitez de l'ombre de la pergola lors de vos seances détente la journée </p>

        </div>
        <div class ='zoneimage1'>
            <img class="zoneimage1-img" src="http://localhost/wordpress/wp-content/uploads/2023/07/terassecheval.jpg">
        </div>

    </div>
</div>


<?php get_footer();?>