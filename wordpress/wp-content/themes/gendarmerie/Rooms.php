<?php
/* Template Name: Roomss */
// Query the ACF archive posts
$args = array(
    'post_type' => 'post',
    'meta_key' => 'chambre_prix', // Replace with your custom field key for sorting
    'orderby' => 'meta_value_num',
    'order' => 'ASC',
    'posts_per_page' => -1,
);

$query = new WP_Query($args);

// Check if there are posts
if ($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post();
        // Get ACF field values
        $titre = get_field('titre');
        $chambre_image = get_field('chambre_image');
        $chambre_capacite = get_field('chambre_capacite');
        $chambre_description = get_field('chambre_description');
        $chambre_prix = get_field('chambre_prix');

        // Output post data
        ?>
        <div class="room">
            <h2><?php echo $titre; ?></h2>
            <?php if ($chambre_image) : ?>
                <img src="<?php echo $chambre_image; ?>" alt="Room Image">
            <?php endif; ?>
            <p>Capacity: <?php echo $chambre_capacite; ?></p>
            <p>Description: <?php echo $chambre_description; ?></p>
            <p>Price: <?php echo $chambre_prix; ?></p>
        </div>
        <?php
    }
}

// Restore original post data
wp_reset_postdata();
?>
