
<?php 
//creating  theme using  site theme editor
if ( ! function_exists( 'gendarmerie_theme_support' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @since 
	 *
	 * @return void
	 */
	function gendarmerie_theme_support() {

		// Add support for block styles.
		add_theme_support( 'wp-block-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style.css' );

	}

endif;
add_action( 'after_setup_theme', 'gendarmerie_theme_support' );
 


//loads stylesheet
function load_css()
{

wp_register_style('bootstrap',get_template_directory_uri().'/css/bootstrap.min.css', array(),false,'all');
wp_enqueue_style('bootstrap');

wp_register_style('main',get_template_directory_uri().'/css/main.css', array(),false,'all');
wp_enqueue_style('main');

}
add_action('wp_enqueue_scripts','load_css');

//loads js

function load_js()
{
wp_enqueue_script('jquery');
wp_register_script('bootstrap',get_template_directory_uri().'/js/bootstrap.min.js', 'jquery',false,true);
wp_enqueue_script('bootstrap');

wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array('jquery'), '1.12.9', true);
}

add_action('wp_enqueue_scripts','load_js');


//theme option
add_theme_support('menus');
add_theme_support('post-thumbnails');


// Register Custom Navigation Menu
function register_top_bar_menu() {
    register_nav_menu('top_bar_menu', 'Top Bar Menu');
}
add_action('after_setup_theme', 'register_top_bar_menu');

//adding logo 
add_theme_support('custom-logo');

// adding custom background
add_theme_support( 'custom-background' );

