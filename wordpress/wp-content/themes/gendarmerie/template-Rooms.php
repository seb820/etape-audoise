<?php
/* Template Name: Rooms */
get_header();
?>
<div class="container-noschambres">
    <?php
    $args = array(
        'post_type' => 'post',
        'meta_key' => 'chambre_prix', // Replace with your custom field key for sorting
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'posts_per_page' => -1,
    );
    
    $query = new WP_Query($args);
    

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            // Get ACF field values
            $titre = get_field('titre');
            $chambre_image = get_field('chambre_image');
            $chambre_capacite = get_field('chambre_capacite');
            $chambre_description = get_field('chambre_description');
            $chambre_prix = get_field('chambre_prix');
    
            ?>

            <div class="card1">
                <?php if ($chambre_image) : ?>
                    <img id="nos-chambre-img" src="<?php echo $chambre_image; ?>" class="card-img-top-chambre" alt="<?php echo $titre; ?>">
                <?php endif; ?>
                <div class="card-body-noschambres">
                    <h5 class="card-title-chambre"><?php echo $titre; ?></h5>
                    <p class="card-text-capacite"><?php echo $chambre_capacite; ?></p>
                    <p class="card-text-descrip"><?php echo $chambre_description; ?></p>
                    <p class="card-text-tarif">A partir de <?php echo $chambre_prix; ?>€ la nuit</p>
                </div>
                <div class="footercard1">
                    <a href="<?php the_permalink(); ?>" class="btn-noschambres">Voir la Chambre</a>
                </div>
            </div>

        <?php
        }
        wp_reset_postdata();
    } else {
        echo 'No rooms found.';
    }
    ?>
</div>

<?php
get_footer();
?>
