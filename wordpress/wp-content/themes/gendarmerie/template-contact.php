<?php
/* Template Name: contact */
?>
<?php get_header(); ?>

<h1><?php the_title(); ?></h1>

<div id="contact-info" class="container">
  <?php
  // Display the contact form
  echo do_shortcode('[gendarmerieform]');
  ?>
</div>

<?php get_footer(); ?>
